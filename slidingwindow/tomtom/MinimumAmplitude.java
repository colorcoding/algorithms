package tomtom;

import com.sun.corba.se.impl.ior.FreezableList;

import java.util.Arrays;
import java.util.concurrent.ArrayBlockingQueue;

public class MinimumAmplitude {

    public static void main(String[] args) {
        int[] A = {9, 5, 33, 6, 41};
        System.out.println(solution(A, 4));
    }

    // window size is 2
    public static int solution(int[] A, int k) {

        int minAmp = Integer.MAX_VALUE;

        if((A.length > 3 && A.length < 100000) && (k >= 1 && k <= A.length-1)){
            int startIndex = 0;
            int endIndex = startIndex + k;
            int amp = 0;
            int[] resultant = new int[A.length - k];

            for(int i = 0; i <= A.length-k; i++) {
                startIndex = i;
                endIndex = i + k -1;

                calculateResultantArray(A, startIndex, endIndex, resultant);
                Arrays.sort(resultant);

                if(resultant.length == 1){
                    amp = resultant[0];
                } else {
                    amp = resultant[resultant.length-1] - resultant[0];
                }
                if(amp < minAmp){
                    minAmp = amp;
                }
            }
        }

        return minAmp;
    }

    public static void calculateResultantArray(int[] A, int startIndex, int endIndex, int[] resultant) {
        int index = 0;

        for(int i = 0; i < A.length; i ++) {
            if (startIndex <= i  &&  i <= endIndex) {
                continue;
            } else {
                resultant[index] = A[i];
                index++;
            }
        }
    }
}
