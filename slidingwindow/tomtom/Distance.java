package tomtom;

import java.util.*;
import java.lang.*;

public class Distance {
//Retail store wants to expand into a new neighbourhood
        public int solution(int K, int[][] A) {

            int emptyPlots = 0;

            // Check an empty plot and if it's distance from each house is within K
            for(int i = 0; i < A.length; i ++){
                for(int j = 0; j < A[i].length; j++){
                    if(A[i][j] == 0){
                        if(isDistanceWithinK(A, K, i, j)){
                            emptyPlots++;
                        }
                    }
                }
            }

            return emptyPlots;
        }

        public boolean isDistanceWithinK(int[][] A, int K, int row, int col) {

            boolean distanceWithinK = true;
            for (int i = 0; i < A.length; i++) {
                for (int j = 0; j < A[i].length; j++) {
                    if (A[i][j] == 1) {
                        // calculate the house distance from cell[row,col]
                        if (getDistance(i, j, row, col) > K) {
                            return false;
                        }
                    }
                }

            }
            return distanceWithinK;
        }

        // Get absolute distance between 2 cells
        public int getDistance(int x1, int y1, int x2, int y2){
            return Math.abs(x2 - x1) + Math.abs(y2 - y1);
        }

}
