package leetcode;

/**
 * Longest Substring without repeating characters
 * Leetcode problem #3
 */
public class LongestSubstring {

    /**
     *
     * Using Sliding window technique
     */
    public static int lengthOfLongestSubstring(String s) {
        char[] input = s.toCharArray();
        String substring = "";
        // window size
        int windowSize = 1;
        int index = 0;
        int windowStartIndex = 0;

        /**
         * e.g. abcabcbb
         * c = a; substring = a; ws = 2; i = 1
         * c = b; substring = ab; ws = 3; i = 2
         * c = c; substring = abc; ws = 4; i = 3
         * c = a; substring = bca; ws = 3; wsi = 1;
         * c = b; substring = cab; ws = 2; wsi =
         */

        while(index < s.length()){
            // Check if char is already present
            String c = Character.toString(s.charAt(index));
            if(substring.contains(c)){
                windowSize = substring.length();
                windowStartIndex = substring.indexOf(c) + 1;
                substring = s.substring(windowStartIndex, windowStartIndex + windowSize);
                index = windowStartIndex + windowSize - 1;
            } else {
                substring = substring + c;
                windowSize++;
                index++;
            }
            System.out.println(substring);
        }

        return substring.length();
    }

    public static void main(String[] args) {
        System.out.println(lengthOfLongestSubstring("abcabcbb"));
    }
}
