package leetcode.variable;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class LongestSubstringKDistinct {


    public static int lengthOfLongestSubstringKDistinct(String s, int k) {

        if( k == 0) {
            return 0 ;
        }
        // HashMap to store the occurrence of characters
        // Size should not exceed k i.e. k distinct characters
        Map<Character, Integer> oMap = new HashMap<>();

        // HashSet to store the substrings
        Set<String> resultSet = new HashSet<>();

        // String to create substring
        String window = "";
        for (int i = 0; i < s.length(); i++) {

            Character c = s.charAt(i);

            // Check the size of oMap
            if (oMap.get(c) == null) {
                if (oMap.size() < k) {
                    window = window + s.charAt(i);
                    increaseCharacterCount(oMap, c);
                } else {
                    // Window is now a substring with K distinct characters
                    // Put it in Set
                    resultSet.add(window);
                    // eliminate the characters from window one by one until size < 3
                    window = eliminatePreviousNonDistinctCharacters(window, oMap, k);
                    window = window + s.charAt(i);
                    increaseCharacterCount(oMap, c);
                }
            } else {
                // The character is repeating
                window = window + s.charAt(i);
                increaseCharacterCount(oMap, c);
            }
        }
        // Add the last substring
        resultSet.add(window);
        return getLongestSubstring(resultSet).length();
    }

    private static String getLongestSubstring(Set<String> resultSet) {
        String result = "";
        for (String s : resultSet){
            System.out.println(s);
            if(s.length() > result.length()) {
                result = s;
            }
        }
        return result;
    }

    private static String eliminatePreviousNonDistinctCharacters(String window, Map<Character, Integer> oMap, int size){
        while(window.length() > 0) {
            Character c = window.charAt(0);
            window = window.substring(1, window.length());
            decreaseCharacterCount(oMap, c);
            if(oMap.size() < size) {
                return window;
            }
        }
        return window;
    }

    private static void decreaseCharacterCount(Map<Character, Integer> oMap, Character c){
        int count = oMap.get(c);
        if(count == 1) {
            oMap.remove(c);
        } else {
            oMap.put(c, count - 1);
        }
    }

    private static void increaseCharacterCount(Map<Character, Integer> oMap, Character c){
        int count;
        if(oMap.get(c) == null){
            oMap.put(c, 1);
            return;
        }
        count = oMap.get(c);
        oMap.put(c, count + 1);
    }

    public static void main(String[] args) {
        System.out.println("Length of longest substring is "+
                lengthOfLongestSubstringKDistinct("a", 0));
    }
}
