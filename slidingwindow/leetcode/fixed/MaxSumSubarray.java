package leetcode.fixed;

public class MaxSumSubarray {

    public static int maxSumSubarray(int[] nums, int k) {

        int sum = 0;
        int i = 1; // start index = i
        int j = k+i-1; // end index = j   // k = j-i+1
        int maxSum = getInitialSum(nums, k);

        // Loop until end of array is
        while( j < nums.length) {
            sum = sum - nums[i-1] + nums[j];
            if(sum > maxSum) {
                maxSum = sum;
            }
            i++;
            j = k+i-1;
        }

        return maxSum;
    }

    private static int getInitialSum(int[] nums, int windowSize) {
        int sum = 0;
        for(int i = 0; i < windowSize; i++) {
           sum = sum + nums[i];
        }
        return sum;
    }

    public static void main(String[] args) {
        int[] input = {1,3,-1,-3,5,3,6,7};
        System.out.println(maxSumSubarray(input, 3));
    }
}
