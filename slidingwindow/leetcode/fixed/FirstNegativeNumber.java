package leetcode.fixed;

public class FirstNegativeNumber {

    public static void printFirstNegative(int[] nums, int k) {

        int i = 0; // start index
        int j = i + k - 1; // end index
        int firstNegativeNumber = 0;

        while( j < nums.length) {

            if(i ==0) {
                firstNegativeNumber = getFirstNegative(nums, 0, k-1);
            } else {
                // If previous subarray had no negative
                // only check negativity of new element
                if(firstNegativeNumber == 0 && nums[j] < 0){
                    firstNegativeNumber = nums[j];
                } else {
                    if(nums[i-1] == firstNegativeNumber) {
                        firstNegativeNumber = getFirstNegative(nums, i, j);
                    } // else firstNegativeNumber continues to remain firstNegativeNumber even for a new subarray
                }
            }
            i++;
            j = i + k -1;
            System.out.println(firstNegativeNumber);
        }

    }

    private static int getFirstNegative(int[] nums, int startIndex, int endIndex) {
        int firstNegative = 0;
        for(int i = startIndex; i <= endIndex; i++) {
            if(nums[i] < 0) {
                return nums[i];
            }
        }
        return firstNegative;
    }

    public static void main(String[] args) {
        int[] input = {12, -1, -7, 8, -15, 30, 16, 28};
        printFirstNegative(input, 3);
    }
}
