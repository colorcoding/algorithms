package leetcode.fixed;

/**
 * Leetcode #1423
 * Maximum Points You can obtain from Cards
 */
public class MaximumPoints {

    public static int maxScore(int[] cardPoints, int k) {

        int bestScore = 0;
        int score = 0;
        int slidingCount = 0;

        if(k==cardPoints.length) {
            return getSum(cardPoints, 0, cardPoints.length-1);
        }

        // Start from beginning
        bestScore = getSum(cardPoints, 0, 0+k-1);
        score = bestScore;
        System.out.println(score);

        while(slidingCount < k){
            score = getScore(score, cardPoints, k-1-slidingCount, cardPoints.length-1-slidingCount);
            System.out.println(score);
            slidingCount++;
            if(score > bestScore) {
                bestScore = score;
            }
        }
        return bestScore;
    }

    private static int getScore(int score, int[] cardPoints, int subtract, int add) {
        return score - cardPoints[subtract] + cardPoints[add];
    }

    private static int getSum(int[] cardPoints, int startIndex, int endIndex) {
        int sum = 0;
        for(int i = startIndex; i <= endIndex; i++) {
            sum = sum + cardPoints[i];
        }
        return sum;
    }

    public static void main(String[] args) {
        //int[] cardPoints = {4, 5, 9, 16, 2, 10, 26, 7, 31, 5, 12, 3};
        int[] cardPoints = {1, 79, 80, 1, 1, 1, 200, 1};
        System.out.println(maxScore(cardPoints, 3));
    }
}
