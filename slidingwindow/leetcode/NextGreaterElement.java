package leetcode;

import java.util.Stack;

public class NextGreaterElement {

    /**
     * Print only the NGE
     */
    public static void printNextGreaterElement(int[] input) {
        Stack<Integer> stack = new Stack<>();
        for(int i = 0; i < input.length-1; i++) {
            if(input[i+1] > input[i]){
                System.out.println("NGE of " + input[i] + " is :" + input[i+1]);
                // Check this condition for elements in stack
                checkNgeInStack(stack, input[i+1]);
            } else {
                stack.push(input[i]);
            }
        }
        stack.push(input[input.length-1]);
        // All remaining elements in Stack doesn't have NGE
        assignNgeToRemnantsInStack(stack);
    }

    private static void assignNgeToRemnantsInStack(Stack<Integer> stack) {
        while(stack.size() > 0) {
            System.out.println("NGE of " + stack.pop() + " is :" + -1);
        }
    }

    private static void checkNgeInStack(Stack<Integer> stack, int nextElement) {
        while(stack.size() > 0) {
            int stackEl = stack.peek();
            if(nextElement > stackEl){
                System.out.println("NGE of " + stack.pop() + " is :" + nextElement);
            } else {
                break;
            }
        }
    }

    public static void main(String[] args) {
        int[] input = {3,2,1,1,4,5,2,3,6};
        printNextGreaterElement(input);
        //checkingCollectionModificationException();
    }

    private static void checkingCollectionModificationException() {
        Stack<Integer> test = new Stack();
        test.push(1);
        test.push(2);
        test.push(3);
        test.push(4);
        test.push(5);

        // This loop throws Concurrent Modification Exception
//        for(Integer i : test){
//            if( i > 2) {
//                test.pop();
//            }
//        }

        // This loop doesn't
        int i = 0;
        while(test.size() > 0) {
            if(test.peek() > 2) {
                test.pop();
            }
            i++;
        }

        // the first code is using an iterator so modifying the collection is not allowed.
        // The second code you are accessing each object with x.get(i), so not using an iterator,
        // modifications thus are allowed
    }
}
