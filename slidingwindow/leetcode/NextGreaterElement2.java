package leetcode;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

/**
 * Leetcode problem 496
 */
public class NextGreaterElement2 {

    public static int[] nextGreaterElement(int[] nums1, int[] nums2) {

        int[] output = new int[nums1.length];
        // Store the NGE in Hashmap
        Map<Integer, Integer> ngeMap = new HashMap<>();
        getNgeInMap(nums2, ngeMap);

        // Query hashmap for nge
        for(int i = 0; i < nums1.length; i++){
            if(ngeMap.get(nums1[i]) != null){
                output[i] = ngeMap.get(nums1[i]);
            }
        }

        return output;
    }

    public static void getNgeInMap(int[] input, Map<Integer, Integer> ngeMap) {
        Stack<Integer> stack = new Stack<>();
        for(int i = 0; i < input.length-1; i++) {
            if(input[i+1] > input[i]){
                ngeMap.put(input[i], input[i+1]);
                // Check this condition for elements in stack
                checkNgeInStack(stack, input[i+1], ngeMap);
            } else {
                stack.push(input[i]);
            }
        }
        stack.push(input[input.length-1]);
        // All remaining elements in Stack doesn't have NGE
        assignNgeToRemnantsInStack(stack, ngeMap);
    }

    private static void checkNgeInStack(Stack<Integer> stack, int nextElement, Map<Integer, Integer> ngeMap) {
        while(stack.size() > 0) {
            int stackEl = stack.peek();
            if(nextElement > stackEl){
                ngeMap.put(stack.pop(), nextElement);
            } else {
                break;
            }

        }
    }

    private static void assignNgeToRemnantsInStack(Stack<Integer> stack, Map<Integer, Integer> ngeMap) {
        while(stack.size() > 0) {
            ngeMap.put(stack.pop(), -1);
        }
    }
}
