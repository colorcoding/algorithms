package com.problems.netflix;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class AnagramHashingMovieTitlesCopy {

    // Hashmap with Key as hashed title and value as title
    public Map<String, String> hashedTitle = new HashMap<>();

    public static void main(String[] args) {

        com.problems.netflix.AnagramHashingMovieTitlesCopy obj = new com.problems.netflix.AnagramHashingMovieTitlesCopy();
        obj.hashedTitle.put(getSortedStringAsKey("The pursuit of Happyness"),"The pursuit of Happyness");
        obj.hashedTitle.put(getSortedStringAsKey("Forrest Gump"),"Forrest Gump");
        obj.hashedTitle.put(getSortedStringAsKey("Jo Jeeta Wohi Sikandar"),"Jo Jeeta Wohi Sikandar");
        obj.hashedTitle.put(getSortedStringAsKey("Kapoor and Sons"),"Kapoor and Sons");

        System.out.println(obj.hashedTitle.get(getSortedStringAsKey("Kpaoor and Sons")));
        System.out.println(obj.hashedTitle.get(getSortedStringAsKey("Jo Jetea Whoi Sikandar")));
        System.out.println(obj.hashedTitle.get(getSortedStringAsKey("The purstui of Happynses")));
        System.out.println(obj.hashedTitle.get(getSortedStringAsKey("Forrtes Gump")));


    }

    private static String getSortedStringAsKey(String title){
        return Stream.of(title.split(" "))
                .map(com.problems.netflix.AnagramHashingMovieTitlesCopy::sorted)
                .collect(Collectors.joining(" "));
    }

    private static String sorted(String s){
        char[] sortedStr = s.toCharArray();
        Arrays.sort(sortedStr);
        return new String(sortedStr);
    }
}
