package com;

public class DoublyLinkedList<T> {

    public class Node{
        public T data;
        public Node next;
        public Node previous;
    }

    public Node head;

    public DoublyLinkedList(){
        this.head = null;
    }

    // Check if list is empty
    public boolean isEmpty(){
        if(head == null){
            return true;
        }
        return false;
    }

    public void insertAtHead(T data){
        Node newNode = new Node();
        newNode.data = data;
        newNode.next = head;
        newNode.previous = null;

        if(!this.isEmpty()){
            this.head.previous = newNode;
        }
        this.head = newNode;

    }

    public void deleteAtHead(){
        // Head node needs to be deleted
        // Next node of head node will become Head node
        // Previous node of the next node should become null

        if(isEmpty())
            return;

        Node n = head.next;
        n.previous = null;
        this.head = n;
    }

    public int getSize(){
        int size = 0;
        if(isEmpty())
            return size;

        Node current = head;
        while(current != null) {
            size++;
            current = current.next;
        }
        return size;
    }
}
