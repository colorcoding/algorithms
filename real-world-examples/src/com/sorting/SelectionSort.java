package com.sorting;

import java.util.Arrays;

public class SelectionSort {

    public static void main(String[] args) {

        int[] arr = {64, 25, 12, -22, 11};

        // Ascending
        // Find out the smallest element from the array and place it to the left
        int smallestNumIndex;
        for(int i = 0; i < arr.length; i++){
            smallestNumIndex = i;
            for(int j = i+1; j < arr.length; j++){
                if(arr[j] < arr[smallestNumIndex]) {
                    smallestNumIndex = j;
                }
            }
            // swap the smallestNum with arr[i]
            int temp = arr[i];
            arr[i] = arr[smallestNumIndex];
            arr[smallestNumIndex] = temp;
        }

        Arrays.stream(arr).forEach(System.out::println);
    }
}
