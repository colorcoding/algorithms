package com.sorting;

import java.util.Arrays;

public class InsertionSort {

    public static void main(String[] args) {
        int[] arr = {64, 25, 12, 22, 11};


        for(int i = 1; i < arr.length; i++){
            int ptrValue = arr[i];
            int ptrIndex = i;
            for(int j = i-1; j >= 0; j--) {
                if(ptrValue < arr[j]) {
                    int temp = arr[j];
                    arr[j] = ptrValue;
                    arr[ptrIndex] = temp;
                    ptrIndex = j;
                }
            }
        }
        Arrays.stream(arr).forEach(System.out::println);
    }
}
