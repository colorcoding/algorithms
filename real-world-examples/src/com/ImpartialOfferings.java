package com;

import java.util.Arrays;

public class ImpartialOfferings {

    public static void main(String[] args) throws Exception {

        if(args != null && args.length > 0){
            for(int i = 2; i < args.length; i=i+2){
                int petSize = Integer.parseInt(args[i-1]);
                int[] pets = getIntArray(args[i].split(" "));
                int totalTreats = treatOffering(pets);
                System.out.println("Case #"+petSize+":"+" "+totalTreats);
            }
        } else {
            throw new Exception("Invalid input arguments");
        }
    }

    public static int[] getIntArray(String[] pets){
        int size = pets.length;
        int [] arr = new int [size];
        for(int i=0; i<size; i++) {
            arr[i] = Integer.parseInt(pets[i]);
        }
        return arr;
    }

    public static int treatOffering(int[] petSizes){
        int numOfPets = petSizes.length;

        // Sort the petSize
        Arrays.sort(petSizes);

        int totalTreats = 0;
        int pointerSize = petSizes[0];
        int petTreat = 1;

        for(int i = 0; i < numOfPets; i++){
            if(petSizes[i] > pointerSize) {
                petTreat++;
                pointerSize = petSizes[i];
            }
            totalTreats = totalTreats + petTreat;
        }
        return totalTreats;
    }
}
