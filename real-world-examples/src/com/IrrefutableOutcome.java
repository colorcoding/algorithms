package com;

public class IrrefutableOutcome {

    public static void main(String[] args) throws Exception {
        if(args != null && args.length > 0){
            for(int i = 1; i < args.length; i=i++){
                findWinner(args[i].toCharArray(), i);
            }
        } else {
            throw new Exception("Invalid input arguments");
        }
    }

    public static void findWinner(char[] input, int testCaseNum){
        int l = input.length-1;
        for(int i=0; i <= l; i++){
            if(!removePiece(input, 'I')){
                // No move possible for Izabella, Olga wins
                System.out.println("Case #"+testCaseNum+":"+" "+"O"+" "+input.length+1);
            }
            if(!removePiece(input, 'O')){
                // No move possible for Olga, Isabella wins
                System.out.println("Case #"+testCaseNum+":"+" "+"I"+" "+input.length+1);
            }
        }
    }

    public static boolean removePiece(char[] input, char s){
        if(input[0] == s || input[input.length-1] == s){
            if(input[1] == s){
                // Remove input[0]

            } else if(input[input.length-2] == s){
                // Remove input[input.length-1]
            }
            // Remove any input[0] OR input[input.length-1]

            return true;
        } else {
            // No move possible
            return false;
        }
    }
}
