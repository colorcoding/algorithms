package com;

public class StringReverseSentence {

    public static void main(String[] args) {
        String s = "The quick brown fox jumped over the lazy dog";
        //reverseNotInPlace(s);
        stringReverse(s.toCharArray());
    }

    // Trying with space complexity O(1)
    public static void stringReverse(char[] s){
        ReverseArray.reverse(s);


    }

    // Time complexity O(n)
    // Space complexity O(n)
    public static void reverseNotInPlace(String s){
        String[] sArr = null;
        StringBuffer revStr = new StringBuffer();

        if(s != null || !s.equals(" ")){
            sArr =  s.split(" ");
            revStr.append(sArr[sArr.length-1]);
        }

        for(int i = sArr.length-2; i >= 0 ; i--){
            revStr.append(" ");
            revStr.append(sArr[i]);
        }
        System.out.println(revStr.toString());

    }
}
