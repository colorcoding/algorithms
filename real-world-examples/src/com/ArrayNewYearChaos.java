package com;

import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class ArrayNewYearChaos {

    public static void main(String[] args) {
        minimumBribes(new int[]{1, 2, 5, 3, 7, 8, 6, 4});
    }

    // 1, 2, 3, 4, 5, 6, 7, 8
    // 2 swaps
    // 1, 2, 3, 4, 5, 6, 8, 7 --> 1, 2, 3, 4, 5, 8, 6, 7

    // Complete the minimumBribes function below.
    static void minimumBribes(int[] arr) {
        int swap = 0;
        for(int i = arr.length-1; i >= 0; i--){
            // Check if bribe has happened
            if(arr[i] != i+1){
                // Check if swap is 1
                if(arr[i-1] == i+1){
                    swap++;
                    swap(arr,i,i-1);
                } // Check if swap is 2
                else if(arr[i-2] == i+1) {
                    swap += 2;
                    swap(arr,i,i-2);
                    swap(arr,i-1,i-2);
                } else {
                    System.out.println("Too chaotic");
                    return;
                }
            }
        }
        System.out.println(swap);
    }

    static void swap(int[] arr, int from, int to){
        int temp = arr[to];
        arr[to] = arr[from];
        arr[from] = temp;
    }

//    private static final Scanner scanner = new Scanner(System.in);
//
//    public static void main(String[] args) {
//        int t = scanner.nextInt();
//        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");
//
//        for (int tItr = 0; tItr < t; tItr++) {
//            int n = scanner.nextInt();
//            scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");
//
//            int[] q = new int[n];
//
//            String[] qItems = scanner.nextLine().split(" ");
//            scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");
//
//            for (int i = 0; i < n; i++) {
//                int qItem = Integer.parseInt(qItems[i]);
//                q[i] = qItem;
//            }
//
//            minimumBribes(q);
//        }
//
//        scanner.close();
//    }
}

