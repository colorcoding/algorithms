package com;

public class HackerrankMinimumSwaps {

    public static void main(String[] args) {
        System.out.println(minimumSwaps(new int[]{2, 3, 4, 1, 5}));
    }

    // Terminated due to Timeout in Hackerank due to O(n2) complexity
    static int minimumSwaps(int[] arr) {
        int countSwaps = 0;
        for(int i = 0; i < arr.length; i++){
            if(arr[i] != i+1){
                for(int j = i; j < arr.length; j++){
                    if(arr[j] == i+1){
                        swap(arr, i, j);
                        countSwaps++;
                    }
                }
            }
        }
        return countSwaps;
    }

    static int minimumSwapsOptimum(int[] arr){
        int countSwaps = 0;
        return countSwaps;
    }

    static void swap(int[] arr, int from, int to){
        int temp = arr[to];
        arr[to] = arr[from];
        arr[from] = temp;
    }

}
