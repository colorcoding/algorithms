package com;

public class ReverseArray {

    public static void main(String[] args) {

        System.out.println(reverse("Gayatri".toCharArray()));
        System.out.println(reverse("Harsha".toCharArray()));

    }

    // Reverse String in place - space complexity O(1)
    public static char[] reverse(char[] s){
        int l = s.length - 1;
        for(int i = 0; i <= l; i++){
            if(i <= l/2){
                char temp = s[i];
                s[i] = s[l-i];
                s[l-i] = temp;
            }
        }
        return s;
    }
}
