package com;

public class DoublyLinkedListWithTail<T> {

    public class Node {
        public T data;
        public Node prev;
        public Node next;
    }

    public Node head;
    public Node tail;

    public DoublyLinkedListWithTail(){
        this.head = null;
        this.tail = null;
    }

    public boolean isEmpty() {
        if(this.head == null && this.tail == null){
            return true;
        }
        return false;
    }

    public void insertNodeAtHead(T data) {
        Node newNode = new Node();
        newNode.data = data;
        newNode.next = head;
        newNode.prev = null;

        // Scenario: when this object (DLL) is NOT empty
        if(!isEmpty())
            head.prev = newNode;
        else // When this object (DLL) has no head and tail
            tail = newNode;

        head = newNode;
    }

    public void insertNodeAtTail(T data) {
        Node newNode = new Node();
        newNode.data = data;
        newNode.next = null;
        newNode.prev = tail;

        // Scenario: when this object (DLL) is not Empty
        if(!isEmpty())
            tail.next = newNode;
        else // When this object (DLL) has no head and tail
            head = newNode;

        tail = newNode;

    }

    public void deleteNodeAtHead() {

    }

    public void deleteNodeAtTail() {

    }
}
