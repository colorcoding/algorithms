package com.problems;

import java.util.HashMap;
import java.util.Map;

public class GoogleCodeJam2020Prob1 {

    // Interleaved output
    public static void main(String[] args) {
        String s = "IiOOIo";
        getIOOccurrence(s);

    }

    public static String getIOOccurrence(String s) {
        Map<Integer, Character> iMap= new HashMap<>();
        Map<Integer, Character> oMap= new HashMap<>();
        int iCounter = 0;
        int oCounter = 0;

        for(int i = 0; i < s.length(); i++){
            Character c = s.charAt(i);
            if(c.equals('I') || c.equals('i')){
                iMap.put(iCounter, s.charAt(i));
                iCounter++;
            } else {
                oMap.put(oCounter, s.charAt(i));
                oCounter++;
            }
        }

        int counter = 0;
        for(int index: iMap.keySet()){
            if(iMap.get(index).equals('I') &&
                    oMap.get(index).equals('O') ){ // Check capital I
                counter++;
            }
        }

        return "s";
    }
}
