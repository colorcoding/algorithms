package com.problems.netflix;

import java.util.Arrays;

public class ProductArray {

    public static void main(String[] args) {

        int[] arr = {1,2,3,4};

        int product = arr[0];
        for(int i = 0; i < arr.length; i++){
            if(arr[i] != 0){
                product = product * arr[i];
            }
        }
        System.out.println("The product of the array is: " + product);

        int[] productArr = new int[arr.length];
        for(int i = 0; i < arr.length; i++){
            productArr[i] = product/arr[i];
        }
        Arrays.stream(productArr).forEach(System.out::println);
    }
}
