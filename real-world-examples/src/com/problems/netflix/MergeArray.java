package com.problems.netflix;

import java.util.Arrays;

public class MergeArray {

        // merge arr1 and arr2 into a new result array
        public static int[] mergeArrays(int[] arr1, int[] arr2)
        {
            int l1 = arr1.length;
            int l2 = arr2.length;
            int i = 0;
            int j = 0;
            // write your code here
            int[] result = new int[l1+l2];

            while(i<arr1.length && j<arr2.length){
                if(arr1[i] < arr2[j]){
                    result[i+j] = arr1[i];
                    i++;
                } else {
                    result[i+j] = arr2[j];
                    j++;
                }
            }

            while(i < arr1.length){
                result[i+j] = arr1[i];
                i++;
            }

            while(j < arr2.length){
                result[i+j] = arr2[j];
                j++;
            }

            return result; // make a new resultant array and return your results in that
        }

    public static void main(String[] args) {
        int[] arr1 = {23, 76, 91, 105, 169};
        int[] arr2 = {11, 41, 54, 99, 210};
        int[] result = mergeArrays(arr1, arr2);
        Arrays.stream(result).forEach(System.out::println);
    }
}
