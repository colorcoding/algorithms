package com.problems.netflix;

public class FindTwoNumbers {

    public static void main(String[] args) {

        //

        int num = 13;

        // Assuming the array is sorted
        int[] arr = {-1,3,5,6,7,14,21,60};

        int ptrLeft = 0;
        int ptrRight = arr.length-1;

        while(ptrLeft < ptrRight){

            // Break if first element is greater or equal to num
            if(arr[0] >= num){
                break;
            }

            // Keep shifting left the ptrRight if arr[ptrRight] is greater than the num
//            if(arr[ptrRight] >= num){
//                ptrRight--;
//                continue;
//            }

            if(arr[ptrLeft] + arr[ptrRight] == num){
                System.out.println(" The numbers adding to num are: " + arr[ptrLeft] + " and " + arr[ptrRight]);
                break;
            } else if(arr[ptrLeft] + arr[ptrRight] < num){
                ptrLeft++;
            } else {
                ptrRight--;
            }

        }
        System.out.println("Left pointer: " + ptrLeft);
        System.out.println("Right pointer: " + ptrRight);

    }


}
