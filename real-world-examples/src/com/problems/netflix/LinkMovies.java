package com.problems.netflix;

import sun.awt.image.ImageWatched;

public class LinkMovies {

    private int rank;
    private String Title;
    private LinkMovies next;

    public LinkMovies(){

    }

    public LinkMovies(int rank, String title, LinkMovies next) {
        this.rank = rank;
        this.Title = title;
        this.next = next;
    }

    public void insertNodeAtTail(LinkMovies node) {
        LinkMovies temp = this;
        while(temp.getNext() != null){
            temp = temp.getNext();
        }
        temp.setNext(node);
    }

    public void insertNodeAtRight(LinkMovies node){
        LinkMovies temp = this.next;
        this.next = node;
        node.setNext(temp);
    }

    public LinkMovies insertNodeAtLeft(LinkMovies node){
        LinkMovies newList = new LinkMovies(node.getRank(), node.getTitle(), this);
        return newList;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public LinkMovies getNext() {
        return next;
    }

    public void setNext(LinkMovies next) {
        this.next = next;
    }

    @Override
    public String toString() {
        return "LinkMovies{" +
                "rank=" + rank +
                ", Title='" + Title + '\'' +
                ", next=" + next +
                '}';
    }
}
