package com.problems.netflix;

public class MergeSortedLists {
    public static void main(String[] args) {

        LinkMovies list1 = new LinkMovies(11, "Lion King",
                new LinkMovies(23, "White Tiger",
                        new LinkMovies(32, "Inception",
                                new LinkMovies(38, "Speed", null))));

        LinkMovies list2 = new LinkMovies(1, "Tenet",
                new LinkMovies(19, "Top Gun",
                        new LinkMovies(57, "Frozen",
                                new LinkMovies(99, "Jumanji", null))));

        // Test insertion in head node
//        list1 = insertHeadNode(new LinkMovies(1, "Tenet", null), list1);
//        System.out.println(list1.toString());

        // Test insertion in middle
//        list1.getNext().getNext().getNext().insertNodeAtRight(new LinkMovies(57, "Frozen",null));
//        System.out.println(list1.toString());

         mergeAndSort(list1, list2);
    }

    public static void mergeAndSort(LinkMovies l1, LinkMovies l2){
        LinkMovies mergedList = null;
        LinkMovies p1 = l1;
        LinkMovies p2 = l2;

        while(p1 != null && p2 != null){
            if(p1.getRank() < p2.getRank()){
                mergedList = addNode(mergedList, new LinkMovies(p1.getRank(), p1.getTitle(), null));
                p1 = p1.getNext();
            } else {
                mergedList = addNode(mergedList, new LinkMovies(p2.getRank(), p2.getTitle(), null));
                p2 = p2.getNext();
            }
        }
        if(p1 != null){
            mergedList.insertNodeAtTail(p1);
        }
        if(p2 != null){
            mergedList.insertNodeAtTail(p2);
        }
        System.out.println(mergedList);
    }

    public static LinkMovies addNode(LinkMovies mergedList, LinkMovies node){
        if (mergedList != null){
            mergedList.insertNodeAtTail(node);
        } else {
            mergedList = new LinkMovies(node.getRank(), node.getTitle(), null);
        }
        return mergedList;
    }

    public static LinkMovies insertHeadNode(LinkMovies head, LinkMovies list){
        head.setNext(list);
        return head;
    }
}