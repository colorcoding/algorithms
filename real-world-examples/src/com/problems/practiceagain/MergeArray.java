package com.problems.practiceagain;

import java.util.Arrays;

public class MergeArray {

    public static void main(String[] args) {

        int[] arr1 = {23, 76, 91, 105, 169};
        int[] arr2 = {11, 41, 54, 99, 210};

        // Merge the sorted arrays into the third array - result is sorted
        // Space complexity O(m+n)
        // Time complexity O(n)

        int i = 0;
        int j = 0;
        int[] mergedArray = new int[arr1.length + arr2.length];

        while(i < arr1.length && j < arr2.length){
            if(arr1[i] < arr2[j]){
                mergedArray[i+j] = arr1[i];
                i++;
            } else{
                mergedArray[i+j] = arr2[j];
                j++;
            }
        }
        while(i < arr1.length){
            mergedArray[i+j] = arr1[i];
            i++;
        }
        while(j < arr2.length){
            mergedArray[i+j] = arr2[j];
            j++;
        }

        Arrays.stream(mergedArray).forEach(System.out::println);

    }
}
