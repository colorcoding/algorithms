package graph.arrayofarraylist.dfs;


import graph.arrayofarraylist.Graph;

/**
 * Using Graph as Array of Arraylist of Edges
 * Used to find if there is a path between 2 nodes/ vertices
 */
public class DFS {

    private Graph graph;
    private boolean[] visited;

    public DFS(Graph graph){
        this.graph = graph;
        this.visited = new boolean[graph.adjList.length];
    }

    public boolean hasPath(int source, int destination) {
        if(source == destination){
            return true;
        }

        visited[source] = true;
        // Get all the Edges connected to the source
        for(Graph.Edge edge : this.graph.adjList[source]){
            if(!visited[edge.destination]){
                boolean doesPathExist = hasPath(edge.destination, destination);
                if(doesPathExist == true){
                    return true;
                }
            }
        }

        return false;
    }







//    public boolean hasPath(int source, int destination){
//        // Found the destination
//        if(source == destination) {
//            return true;
//        }
//        // source node is visited now
//        visited[source] = true;
//        //Recursively find if child nodes are connected to destination
//        for(Graph.Edge edge: this.graph.adjList[source]){
//            //Check if node is already visited
//            if(!visited[edge.destination]) {
//                boolean pathExists = hasPath(edge.destination, destination);
//                if(pathExists == true){
//                    return true;
//                }
//            }
//        }
//        return false;
//    }

}
