package graph.arrayofarraylist.dfs;

import graph.arrayofarraylist.Graph;

import java.util.ArrayList;
import java.util.List;

public class ConnectedComponents {

    public Graph graph;

    public boolean[] visited;

    public void printConnectedComponents() {
        List<List<Integer>> connectedComponents = new ArrayList<>();
        for(int i = 0; i < graph.adjList.length; i++) {
            if(!visited[i]){
                // For each vertex, recursively get the connected edges
                List<Integer> connectedVertices = new ArrayList<>();
                getConnectedEdges(i, visited, connectedVertices);
                connectedComponents.add(connectedVertices);
            }
        }

        connectedComponents.stream().forEach(e ->{
            System.out.println();
            e.stream().forEach(System.out::println);
        });
    }

    private List<Integer> getConnectedEdges(int source, boolean[] visited, List<Integer> connectedVertices){
        visited[source] = true;
        connectedVertices.add(source);
        for(Graph.Edge edge : graph.adjList[source]){
            if(!visited[edge.destination]){
                getConnectedEdges(edge.destination, visited, connectedVertices);
            }
        }
        return connectedVertices;
    }

    public static void main(String[] args) {

        Graph graph = new Graph(7);
        getGraph(graph);

        ConnectedComponents connectedComponents = new ConnectedComponents();
        connectedComponents.graph = graph;
        connectedComponents.visited = new boolean[graph.adjList.length];
        connectedComponents.printConnectedComponents();
    }

    private static void getGraph(Graph graph){
        graph.addEdge(new Graph.Edge(0, 1, 0));
        graph.addEdge(new Graph.Edge(2, 3, 0));
        graph.addEdge(new Graph.Edge(4, 5, 0));
        graph.addEdge(new Graph.Edge(4, 6, 0));
        graph.addEdge(new Graph.Edge(5, 6, 0));
    }
}
