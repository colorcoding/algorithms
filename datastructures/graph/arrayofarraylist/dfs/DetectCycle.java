package graph.arrayofarraylist.dfs;

import graph.arrayofarraylist.Graph;
import graph.arrayofarraylist.bfs.BFS;

import java.util.HashMap;
import java.util.Map;

public class DetectCycle {

    Map<Integer, Integer> visitedEdges = new HashMap<>();

    public boolean detectCycle(Graph graph){

        for(int i = 0; i < graph.adjList.length; i++) {
            for(Graph.Edge edge : graph.adjList[i]){

                if(visitedEdges.get(edge.source) != null &&
                        visitedEdges.get(edge.source) != edge.destination){
                    // Vertex is already visited
                    return true;
                }
                visitedEdges.put(edge.source, edge.destination);
            }
        }
        return false;
    }

    public static void main(String[] args) {
        Graph graph = new Graph(7);
        getGraph(graph);

        DetectCycle detectCycle = new DetectCycle();
        detectCycle.detectCycle(graph);
        System.out.println(detectCycle.visitedEdges.toString());
    }

    private static void getGraph(Graph graph){
        graph.addEdge(new Graph.Edge(0, 1, 0));
        graph.addEdge(new Graph.Edge(1, 2, 0));
        graph.addEdge(new Graph.Edge(2, 3, 0));
        graph.addEdge(new Graph.Edge(0, 3, 0));
        graph.addEdge(new Graph.Edge(3, 4, 0));
        graph.addEdge(new Graph.Edge(4, 5, 0));
        graph.addEdge(new Graph.Edge(4, 6, 0));
        graph.addEdge(new Graph.Edge(5, 6, 0));
    }
}
