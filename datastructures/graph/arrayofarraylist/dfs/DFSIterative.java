package graph.arrayofarraylist.dfs;

import graph.arrayofarraylist.Graph2;

import java.util.Stack;

public class DFSIterative {

    private Graph2 graph;

    public boolean hasPath(int source, int destination) {

        Stack<Integer> stack = new Stack<>();
        stack.push(source);
        boolean[] visited = new boolean[this.graph.graph.length];

        while(!stack.empty()){
            Integer vertex = stack.pop();

            if(visited[vertex]){
                continue;
            }
            visited[vertex] = true;

            for(Graph2.Edge edge : this.graph.graph[vertex]){
                if(!visited[edge.destination]) {
                    stack.push(edge.destination);
                }
            }
        }


        return false;
    }

}
