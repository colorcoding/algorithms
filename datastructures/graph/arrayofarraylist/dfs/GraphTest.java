package graph.arrayofarraylist.dfs;

import graph.arrayofarraylist.Graph;

public class GraphTest {

    public static void main(String[] args) {
        Graph graph = new Graph(7);
//        addEdgesToGraph(graph);
//        DFS dfs = new DFS(graph);
//        System.out.println("Is there a path between - " + "source "+ 0 + " and destination " + 6 + dfs.hasPath(0, 6));

        // Detect Cycle in a graph
//        DetectCycle detectCycle = new DetectCycle();
//        System.out.println(detectCycle.detectCycle(graph));
        getGraphForAllPaths(graph);
        AllPaths allPaths = new AllPaths();
        allPaths.graph = graph;
        allPaths.getAllPaths(0, 6);
    }

    private static void addEdgesToGraph(Graph graph) {
        graph.addEdge(new Graph.Edge(0, 1, 0));
        graph.addEdge(new Graph.Edge(0, 3, 0));
        graph.addEdge(new Graph.Edge(1, 0, 0));
        graph.addEdge(new Graph.Edge(1, 2, 0));
        graph.addEdge(new Graph.Edge(2, 1, 0));
        graph.addEdge(new Graph.Edge(2, 3, 0));
        graph.addEdge(new Graph.Edge(2, 5, 0));
        graph.addEdge(new Graph.Edge(3, 0, 0));
        graph.addEdge(new Graph.Edge(3, 2, 0));
        graph.addEdge(new Graph.Edge(4, 5, 0));
        graph.addEdge(new Graph.Edge(4, 6, 0));
        graph.addEdge(new Graph.Edge(5, 2, 0));
        graph.addEdge(new Graph.Edge(5, 6, 0));
        graph.addEdge(new Graph.Edge(5, 4, 0));
        graph.addEdge(new Graph.Edge(5, 6, 0));
    }

    private static void getGraphForAllPaths(Graph graph){
        graph.addEdge(new Graph.Edge(0, 1, 0));
        graph.addEdge(new Graph.Edge(1, 2, 0));
        graph.addEdge(new Graph.Edge(2, 3, 0));
        graph.addEdge(new Graph.Edge(0, 3, 0));
        graph.addEdge(new Graph.Edge(3, 4, 0));
        graph.addEdge(new Graph.Edge(4, 5, 0));
        graph.addEdge(new Graph.Edge(4, 6, 0));
        graph.addEdge(new Graph.Edge(5, 6, 0));
    }
}
