package graph.arrayofarraylist.dfs;

import graph.arrayofarraylist.Graph;

public class GraphConnected {

    private Graph graph;

    /**
     * A Graph is connected if there is a path from every vertex to every other
     * Start recursion from the first vertex to reach other vertex from the connected edge.
     * The last vertex would be the last index of the array of graph.
     * If recursion logic reaches to the last vertex, the graph is connected
     *
     * @return
     */
    private boolean isGraphConnected(){
        return false;
    }

    public static void main(String[] args) {

    }
}
