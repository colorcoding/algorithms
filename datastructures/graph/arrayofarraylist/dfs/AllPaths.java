package graph.arrayofarraylist.dfs;

import graph.arrayofarraylist.Graph;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class AllPaths {

    public Graph graph;

    public List<LinkedList<Integer>> paths = new ArrayList<>();

    public boolean[] visited;

    public void getAllPaths(int source, int destination){
        visited = new boolean[graph.adjList.length];

        // Just print the paths
        printAllPaths(source, destination, visited, source+"");

        // Return all paths in a list
        LinkedList<Integer> path = new LinkedList<>();
        path.add(source);
        getAllPaths(source, destination, visited, path);
    }

    public void printAllPaths(int source, int destination, boolean[] visited, String path){
        if(source == destination){
            // Reached the destination
            System.out.println(path);
            return;
        }

        visited[source] = true;
        for(Graph.Edge edge: graph.adjList[source]){
            if(!visited[edge.destination]){
                // Recursion until destination is reached
                // Imagine what would happen below line is uncommented
                // path = path + edge.destination
                printAllPaths(edge.destination, destination, visited, path+edge.destination);
            }
        }
        visited[source] = false;
    }

    public void getAllPaths(int source, int destination, boolean[] visited,
                            LinkedList<Integer> path){
        if(source == destination){
            // Reached the destination
            justPrintThePath(path);
            return;
        }

        visited[source] = true;
        for(Graph.Edge edge: graph.adjList[source]){
            if(!visited[edge.destination]){
                path.add(edge.destination);
                // Recursion until destination is reached
                getAllPaths(edge.destination, destination, visited, path);
                path.removeLast();
            }
        }
        visited[source] = false;
    }

    private void justPrintThePath(LinkedList<Integer> path){
        System.out.println();
        path.stream().forEach(System.out::print);
    }
}
