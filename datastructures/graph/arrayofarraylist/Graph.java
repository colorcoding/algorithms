package graph.arrayofarraylist;

import java.util.ArrayList;
import java.util.List;

// Reference: Algorithms by Robert Sedgewick and YT - Pepcoding
// Adjacency List representation of Graph - "Array of ArrayList of Edges"
// Useful to represent Weighted graph
public class Graph {

    public List<Edge>[] adjList;

    public Graph(int numberOfVertices){
        this.adjList = new ArrayList[numberOfVertices];
        for(int i = 0; i < numberOfVertices; i++){
            this.adjList[i] = new ArrayList<Edge>();
        }
    }

    public void addEdge(Edge edge){
        if(this.adjList != null){
            int source = edge.source;
            this.adjList[edge.source].add(edge);
        }
    }

    public static class Edge {
        public int source;
        public int destination;
        public int weight;

        public Edge(int source, int destination, int weight) {
            this.source = source;
            this.destination = destination;
            this.weight = weight;
        }
    }
}
