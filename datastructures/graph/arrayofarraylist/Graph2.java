package graph.arrayofarraylist;

import java.util.ArrayList;
import java.util.Arrays;

// Array of arraylist of Edges
public class Graph2 {

    public ArrayList<Edge>[] graph;

    public Graph2(int numberOfVertices){
        this.graph = new ArrayList[numberOfVertices];
        for(int i = 0; i < this.graph.length; i++){
            this.graph[i] = new ArrayList<Edge>();
        }
    }

    // Boundary condition added
    public void addEdge(Edge edge) {
        if(this.graph.length > edge.source){
            this.graph[edge.source].add(edge);
        } else {
            // throw an exception
            System.out.println("Exception!");
        }
    }

    public static void main(String[] args) {
        Graph2 graph2 = new Graph2(5);
        graph2.addEdge(new Edge(4,2,10));
    }

    public static class Edge {
        public int source;
        public int destination;
        public int weight;

        public Edge(int source, int destination, int weight) {
            this.source = source;
            this.destination = destination;
            this.weight = weight;
        }
    }
}
