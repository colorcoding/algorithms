package graph.arrayofarraylist.bfs;

import graph.arrayofarraylist.Graph;

import java.util.ArrayDeque;

public class BFS {

    private Graph graph;
    private boolean[] visited;

    public BFS(Graph graph){
        this.graph = graph;
        this.visited = new boolean[graph.adjList.length];
    }

    /**
     * The queue will be filled up as the unvisited child vertices are discovered
     *
     * @param source
     */
    public void breadthFirstTraverse(int source){

        // Step 1: Create a Queue (FIFO) for vertices to be visited
        ArrayDeque<Element> queue = new ArrayDeque<>();
        // Add the first vertex (from where BFS is started)
        queue.add(new Element(source, source+""));

        while(queue.size() > 0){
            // Remove the first element
            Element el = queue.removeFirst();

            // Check if the element is already visited
            // If yes, no need to mark it again, just continue
            if(visited[el.vertex] == true){
                continue;
            }
            // Mark the element as visited
            visited[el.vertex] = true;

            // Perform work
            System.out.println(el.vertex + " : " + el.path);

            // Add the child/ connected vertex
            for(Graph.Edge edge: graph.adjList[el.vertex]){
                // If the vertex is already added in queue, don't add it again
                if(!visited[edge.destination]){
                    queue.add(new Element(edge.destination, el.path+", "+ edge.destination));
                }
            }
        }
    }

    static class Element {
        int vertex;
        String path;

        Element(int vertex, String path){
            this.vertex = vertex;
            this.path = path;
        }
    }

    public static void main(String[] args) {
        Graph graph = new Graph(7);
        getGraph(graph);

        BFS bfs = new BFS(graph);
        bfs.breadthFirstTraverse(0);
    }

    private static void getGraph(Graph graph){
        graph.addEdge(new Graph.Edge(0, 1, 0));
        graph.addEdge(new Graph.Edge(1, 2, 0));
        graph.addEdge(new Graph.Edge(2, 3, 0));
        graph.addEdge(new Graph.Edge(0, 3, 0));
        graph.addEdge(new Graph.Edge(3, 4, 0));
        graph.addEdge(new Graph.Edge(4, 5, 0));
        graph.addEdge(new Graph.Edge(4, 6, 0));
        graph.addEdge(new Graph.Edge(5, 6, 0));
    }
}
