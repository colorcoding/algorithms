package linkedlist.iteration;

public class CycleDetector {

    public static void main(String[] args) {

    }

    public boolean hasCycle(ListNode head) {

        boolean hasCycle = false;

        // Create a temp node
        ListNode temp = new ListNode(0);

        ListNode current = head;
        ListNode next = null;

        while(current != null){

            if(current.next == temp){
                return true;
            }
            // Save the next node
            next = current.next;

            // Assign the next node as temp
            current.next = temp;

            // Set current node value as next
            current = next;
        }

        return hasCycle;

    }


     static class ListNode {
        int val;
        ListNode next;
        ListNode(int x) {
            val = x;
            next = null;
        }
     }

}
