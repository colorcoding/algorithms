package trees.bst;

import trees.Node;

public class PostOrderTraversal {

    private Node root;

    public void traversePostOrder(Node node) {
        if(node == null){
            return;
        }
        traversePostOrder(node.leftChild);
        traversePostOrder(node.rightChild);
        System.out.println(node.data);
    }

    public static void main(String[] args) {
        PostOrderTraversal iot = new PostOrderTraversal();
        iot.root = iot.getSampleBst();
        iot.traversePostOrder(iot.root);
    }

    public Node getSampleBst() {
        Node root = new Node(20);
        BinarySearchTree  bst = new BinarySearchTree(root);
        bst.addNode(new Node(5));
        bst.addNode(new Node(10));
        bst.addNode(new Node(100));
        bst.addNode(new Node(31));
        bst.addNode(new Node(2));
        return bst.root;
    }

}
