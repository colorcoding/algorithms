package trees.bst;

import trees.Node;

public class BSTSearch {

    public Node root;

    public boolean search(int value){
        return doesExist(value, root);
    }

    public boolean doesExist(int value, Node node) {
        // Terminate condition
        if(node == null) { // Reached leaf
            return false;
        }

        if(value == node.data) {
            return true;
        }

        if(value < node.data) {
            return doesExist(value, node.leftChild);
        } else if(value > node.data){
            return doesExist(value, node.rightChild);
        }

        return false;
    }

    public static void main(String[] args) {
        BSTSearch bstSearch = new BSTSearch();

        BinarySearchTree bst = new BinarySearchTree();
        bstSearch.root = bst.getSampleBst();
        System.out.println(bstSearch.search(10));
    }

    // Time complexity of binary search
    // Avg - O(logn)
    // Worst - O(n) - if the tree is skewed
}
