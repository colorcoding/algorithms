package trees.bst;

import trees.Node;

public class PreOrderTraversal {

    private Node root;

    public void traversePreOrder(Node node) {
        if(node == null){
            return;
        }
        System.out.println(node.data);



        traversePreOrder(node.leftChild);
        traversePreOrder(node.rightChild);
    }

    public static void main(String[] args) {
        PreOrderTraversal iot = new PreOrderTraversal();
        iot.root = iot.getSampleBst();
        iot.traversePreOrder(iot.root);
    }

    public Node getSampleBst() {
        Node root = new Node(20);
        BinarySearchTree  bst = new BinarySearchTree(root);
        bst.addNode(new Node(5));
        bst.addNode(new Node(10));
        bst.addNode(new Node(100));
        bst.addNode(new Node(31));
        bst.addNode(new Node(2));
        return bst.root;
    }

}
