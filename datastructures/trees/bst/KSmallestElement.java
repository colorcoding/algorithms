package trees.bst;

import trees.Node;

import java.util.LinkedList;
import java.util.List;

public class KSmallestElement {

    private Node root;

    public int kthSmallest(Node root, int k) {

        LinkedList<Integer> orderedTree = new LinkedList<Integer>();
        // Traverse the tree from Left node to right node
        getInOrderTraversedTree(root, orderedTree);

        if(orderedTree.size() >= k){
            return orderedTree.get(k);
        }
       return Integer.MIN_VALUE;
    }

    // Recurse
    private void getInOrderTraversedTree(Node node, LinkedList<Integer> orderedList) {
        if(node == null){
            return;
        }
        getInOrderTraversedTree(node.leftChild, orderedList);
        orderedList.add(node.data);
        getInOrderTraversedTree(node.rightChild, orderedList);
    }

    public static void main(String[] args) {
        KSmallestElement obj = new KSmallestElement();
        obj.root = obj.getSampleBst();
        System.out.println("The smallest 3rd element in the tree is: " + obj.kthSmallest(obj.root, 3));
    }

    public Node getSampleBst() {
        Node root = new Node(20);
        BinarySearchTree  bst = new BinarySearchTree(root);
        bst.addNode(new Node(5));
        bst.addNode(new Node(10));
        bst.addNode(new Node(100));
        bst.addNode(new Node(31));
        bst.addNode(new Node(2));
        return bst.root;
    }
}
