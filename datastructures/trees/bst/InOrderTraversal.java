package trees.bst;

import trees.Node;

public class InOrderTraversal {

    private Node root;

    public void traverseInOrder(Node node) {
        if(node == null){
            return;
        }
        traverseInOrder(node.leftChild);
        System.out.println(node.data);
        traverseInOrder(node.rightChild);
    }

    public static void main(String[] args) {
        InOrderTraversal iot = new InOrderTraversal();
        iot.root = iot.getSampleBst();
        iot.traverseInOrder(iot.root);
    }

    public Node getSampleBst() {
        Node root = new Node(20);
        BinarySearchTree  bst = new BinarySearchTree(root);
        bst.addNode(new Node(5));
        bst.addNode(new Node(10));
        bst.addNode(new Node(100));
        bst.addNode(new Node(31));
        bst.addNode(new Node(2));
        return bst.root;
    }

}
