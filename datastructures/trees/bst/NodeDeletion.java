package trees.bst;

import trees.Node;

public class NodeDeletion {

    public Node root;

    private static BSTSearch bstSearch = new BSTSearch();

    public void deleteNode(int data){
        // Check if node exists
        Node result = searchAndDeleteNode(data, root);
    }

    public Node searchAndDeleteNode(int value, Node node){
        // Terminate condition
        if(node == null) { // reached leaf node, node not found
            return null;
        }
        if(value == node.data) {
            // apply delete logic
            if(node.leftChild == null && node.rightChild == null){
                // node to be deleted is a leaf node
                node = null;
            } else if(node.leftChild == null || node.rightChild == null){
                if(node.leftChild == null){
                    node = node.rightChild;
                } else { // right child is null
                    node = node.leftChild;
                }
            } else { // Both children are present
                // Find the left most child of the right subtree
                Node leftMostChild = getLeftChildNode(node.rightChild);
                node.data = leftMostChild.data;
                leftMostChild = null;
            }
            return node;
        }
        if(value < node.data){
            return searchAndDeleteNode(value, node.leftChild);
        } else {
            return searchAndDeleteNode(value, node.rightChild);
        }
    }

    public Node getLeftChildNode(Node node){
        if(node.leftChild == null) {
            return node;
        } else{
            return getLeftChildNode(node.leftChild);
        }
    }

    public static void main(String[] args) {
        BinarySearchTree bst = new BinarySearchTree();

        NodeDeletion nodeDeletion = new NodeDeletion();
        nodeDeletion.root = bst.getSampleBst();
//        nodeDeletion.deleteNode(10);
        System.out.println(nodeDeletion.getLeftChildNode(nodeDeletion.root).data);
    }

    /**
     * Find if node exists, if not return
     * If exists - whether it is a leaf node or parent node
     *
     * Leaf node - Just remove - make parent point to null
     * --> logic same as Binary Search, complexity is O(logn)
     *
     * Parent node -
     * 1. If it has only one child - left or right
     * then just replace the parent with child
     * (The level of node here is not important)
     * --> logic same as Binary Search, complexity is O(logn)
     *
     * 2. If it has both children - left and right
     * then get the left most child of the Right subtree
     * and replace it with parent
     * --> first find the node with BS, then get the left most child of its right subtree
     * Complexity - O(logn) + O(logn)
     */


}
