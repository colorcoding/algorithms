package trees.bst;

import trees.Node;

public class BinarySearchTree {

    public Node root;

    public BinarySearchTree(Node root){
        this.root = root;
    }

    public BinarySearchTree(){
    }

    // Using Recursion
    public void addNode(Node node){
        // Check if the tree has root node or not
        if(this.root == null){
            this.root =  node;
            return;
        }

        // Compare with root node, if less, traverse the left sub tree
        // else right subtree
        addNodeByRecursion(node, this.root);
    }

    private Node addNodeByRecursion(Node node, Node parent){
        if(parent == null){
            return node;
        }
        if(node.data < parent.data){
            parent.leftChild = addNodeByRecursion(node, parent.leftChild);
        } else if(node.data > parent.data){
            parent.rightChild = addNodeByRecursion(node, parent.rightChild);
        } else {
            // if node.data == parent.data i.e. already exists, do nothing
            return parent;
        }
        return parent; // I did this wrong earlier, I wrote return node
    }
    // The time complexity of inserting a node is O(h) = O(logn)

    public void printTree(Node current) {
        if (current == null) return;

        System.out.print(current.data + ",");
        printTree(current.leftChild);
        printTree(current.rightChild);

    }

    public static void main(String[] args) {
        Node root = new Node(20);
        BinarySearchTree  bst = new BinarySearchTree(root);
        bst.addNode(new Node(5));
        bst.addNode(new Node(10));
        bst.addNode(new Node(100));
        bst.addNode(new Node(31));
        bst.addNode(new Node(2));

        bst.printTree(bst.root);

    }

    public Node getSampleBst() {
        Node root = new Node(20);
        BinarySearchTree  bst = new BinarySearchTree(root);
        bst.addNode(new Node(5));
        bst.addNode(new Node(10));
        bst.addNode(new Node(100));
        bst.addNode(new Node(31));
        bst.addNode(new Node(2));
        return bst.root;
    }

}
