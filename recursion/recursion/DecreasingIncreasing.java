package recursion;

public class DecreasingIncreasing {

    public static void main(String[] args) throws Exception {
        // write your code here
        increasingdecreasing(5);
    }

    public static void increasingdecreasing(int n){
        if(n == 0){
            return;
        }
        System.out.println(n);
        increasingdecreasing(n-1);
        System.out.println(n);
    }

    // In recursion
    // 1. Set the breaking condition
    // 2. Correctly write the return statement
    // 3. Decide what needs to be done before recursion call
    // 4. Decide what needs to be done after recursion call
}
