package recursion;

public class GetIndexOfElement {

    public static void main(String[] args) {
        int[] arr = {10,4,-10, 34, 999};
        System.out.println(getIndex(arr, 34, 0));
    }

    private static int getIndex(int arr[], int k, int i){
        if(i >= arr.length){
            return -1;
        }
        if(arr[i] == k){
            return i;
        }
        return getIndex(arr, k, i+1);
    }
}
