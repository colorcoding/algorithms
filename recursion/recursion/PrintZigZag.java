package recursion;

public class PrintZigZag {

    public static void main(String[] args) throws Exception {
        pzz(3);
    }

    public static void pzz(int n){
        if(n == 0){
            return;
        }
        System.out.print(n);
        pzz(n-1);
        System.out.print(n);
        pzz(n-1);
        System.out.print(n);
    }
}
