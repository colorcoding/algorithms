package recursion;

public class TowerOfHanoi {

    public static void main(String[] args) throws Exception {
        toh(3, 10, 11, 12);
    }

    public static void toh(int n, int A, int B, int C){
        if(n == 0){
            return;
        }
        toh(n-1, A, C, B);
        System.out.println(n + "[ "+ A +"--> "+ B + " ]");
        toh(n-1, C, B, A);
    }

}
