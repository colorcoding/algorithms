package recursion;

public class Increasing {

    public static void main(String[] args) {
        increasing(5);
//        decreasing(5);
//        decreasing2(5);
    }

    public static void increasing(int n) {
        if(n == 0){
            return;
        }
        increasing(n-1);
        System.out.println(n);
    }

    public static void decreasing(int n){
        if(n == 0){
            return;
        }
        System.out.println(n);
        decreasing(n-1);
    }
}
