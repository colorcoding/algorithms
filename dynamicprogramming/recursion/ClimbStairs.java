package recursion;

public class ClimbStairs {

    public static void main(String[] args) {
        // count path if 1 stair can be climbed
        System.out.println(countPaths(5));
        // count path if 2 stairs can be climbed
    }

    public static int countPaths(int n) {
        // If reached floor, return 1
        if(n == 0) {
            return 1;
        }
        if(n < 0) {
            return 0;
        }

        int i = countPaths(n-1);
        int j = countPaths(n-2);


        int cp = i + j;
        return cp;
    }
}
