package dp;

public class ClimbStairs {

    public static void main(String[] args) {
        int[] paths = new int[5+1];
        System.out.println(countPaths(5, paths));
    }

    public static int countPaths(int n, int[] paths) {
        // If reached floor, return 1
        if(n == 0) {
            return 1;
        } else if(n < 0) {
            return 0;
        } else if(paths[n] != 0){
            return paths[n];
        }

        int i = countPaths(n-1, paths);
        int j = countPaths(n-2, paths);

        int cp = i + j;
        paths[n] = cp;
        return cp;
    }
}
