package substring;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class CommonSubstringInTwoStrings {


    private static boolean doesStringIntersect(String s1, String s2) {

        Set<Character> characterSet = new HashSet<>();

        for(Character c : s1.toCharArray()){
            characterSet.add(c);
        }

        for(Character c: s2.toCharArray()){
            if(characterSet.contains(c)){
                return true;
            }
        }

        return false;
    }

    public static void main(String[] args) {

        String[] arr1 = {"Hello", "Bye"};
        String[] arr2 = {"World", "Hi"};

        for(int i = 0; i < arr1.length; i++){
            System.out.println(doesStringIntersect(arr1[i], arr2[i]));
        }
    }
}
