public class Swapper {

    // Without using temp variable
    public static void main(String[] args) {
        int a = 10;
        int b = 5;

        System.out.println("Value of a:" + a);
        System.out.println("Value of b:" + b);

        // Swapping
        a = a + b;
        b = a - b;
        a = a - b;

        System.out.println("Value of a after swapping:" + a);
        System.out.println("Value of b after swapping:" + b);
    }
}
