package com.backtracking;

import java.util.Arrays;

public class SeatingArrangement {

    // There are 3 chairs and 3 people - 2 boys and 1 girl.
    // Print all possible seating arrangement

    public static void main(String[] args) {

        int[] people = {1, 2, 3};
        int index = 0;
        int[] pathArr = new int[people.length];
        for(int i = 0; i < people.length; i++){
            pathArr[0] = people[i];
            printPath(people, pathArr, 3, index);
        }
    }

    // Recursive function
    public static void printPath(int[] people, int[] pathArr, int numChairs, int index){
        if(index == numChairs){
            Arrays.stream(pathArr).forEach(System.out::print);
            System.out.println();
            return;
        }
        pathArr[index] = people[index];
        index = index + 1;
        printPath(people, pathArr, numChairs, index);
    }
}
