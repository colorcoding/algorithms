package concurrency.producerconsumer;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class MyBlockingQueueWithLocks {

    // Creating a bounded queue - with size
    private Queue<Integer> queue;
    private int maxSize = 1;
    private ReentrantLock lock = new ReentrantLock(true);
    private Condition empty = lock.newCondition();
    private Condition full = lock.newCondition();

    public MyBlockingQueueWithLocks(int size){
        queue = new LinkedList<>();
        this.maxSize = size;
    }

    public void put(Integer i){
        lock.lock();
        try{
            while(queue.size() == maxSize){
                full.await();
            }
            queue.add(i);
            empty.notifyAll();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }

    public Integer take(){
        lock.lock();
        try{
            while(queue.size() == 0){  // replaced if with why -- as explained in Defog Tech video
                empty.await();
            }
            Integer i = queue.remove();
            full.notifyAll();
            return i;
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
        return -1; // Invalid return
    }
}
