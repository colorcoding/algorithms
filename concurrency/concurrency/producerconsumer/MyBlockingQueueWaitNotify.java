package concurrency.producerconsumer;

import java.util.LinkedList;
import java.util.Queue;

public class MyBlockingQueueWaitNotify {

    private Queue<Integer> queue;
    private int maxSize;

    public MyBlockingQueueWaitNotify(int size){
        this.queue = new LinkedList<>();
        this.maxSize = size;
    }

    public synchronized void put(Integer i){
        try {
            if(queue.size() == maxSize){
                // block the thread
                wait();
            }
            queue.add(i);
            notifyAll();
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public synchronized Integer take(){
        try{
            if(queue.size() == 0){
                wait();
            }
            Integer i = queue.remove();
            notifyAll();
            return i;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return -1; // Invalid return
    }
}
