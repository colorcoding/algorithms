package concurrency.producerconsumer;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadLocalRandom;

public class ProducerConsumer {

    // Using ArrayBlockingQueue
    BlockingQueue<Integer> queue = new ArrayBlockingQueue<>(10);

    class Producer {
        public Runnable produce(){
            final Runnable producer = () -> {
                while(true){
                    try {
                        queue.put(ThreadLocalRandom.current().nextInt());
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            };
            return producer;
        }
    }

    class Consumer{
        public Runnable consume() {
            final Runnable consumer = () -> {
                while(true){
                    try {
                        Integer i = queue.take();
                        System.out.println(i);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            };
            return consumer;
        }
    }

    public static void main(String[] args) {

        ProducerConsumer producerConsumer = new ProducerConsumer();

        // Start Producer thread
        ProducerConsumer.Producer prod = producerConsumer.new Producer();
        new Thread(prod.produce()).start();
        new Thread(prod.produce()).start();

        // Start Consumer thread
        ProducerConsumer.Consumer consumer = producerConsumer.new Consumer();
        new Thread(consumer.consume()).start();
        new Thread(consumer.consume()).start();
    }

}
