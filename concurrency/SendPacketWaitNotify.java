public class SendPacketWaitNotify {

    private String packet;

    private boolean transferred;

    private void send(String data){
        // Wait until the receive is completed
        while(transferred) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        this.packet = data;
        // transfer the packet
        transferred = true;
        notifyAll();
    }

    // Receive only after the packet is sent
    private synchronized String receive(){
        // Wait until the packet is getting transferred
        // When transfer is complete, it comes out of while loop
        while(!transferred) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        transferred = false;
        notifyAll();
        return this.packet;
    }
}
