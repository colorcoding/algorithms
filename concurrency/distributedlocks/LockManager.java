package distributedlocks;

import java.util.ArrayList;
import java.util.List;

public class LockManager {

    // Lock manager --> multiple instances in a distributed system
    // Requirements
    // 1. Multiple threads can request the LockManager to acquire the lock
    // 2. Only one lock is available per shared resource
    // 3. There is a cache where info is stored on which object acquired a lock - consider Hashmap for simplicity
    // 4. Eviction policy should be applied to release the lock

    // Consider this as cache with eviction policy
    private List<String> cache = new ArrayList<>();

    // Called by client
    public void applyLock(String userId) {

    }

    // Called by client
    public void releaseLock() {

    }

}
