public class DiningPhilosophers implements Runnable {

    private int phil;
    private boolean[] forks; // true is available, false is unavailable/ occupied

    public DiningPhilosophers(int phil, boolean[] forks) {
        this.phil = phil;
        this.forks = forks;
    }

    public synchronized void eatFood() throws InterruptedException {
        while(forks[phil-1] == false && forks[phil] == false) {
            wait();
        }
        forks[phil-1] = false;
        forks[phil] = false;
        notifyAll();
    }

    public synchronized void contemplate(){

    }

    public void run() {
        //eatFood();
        contemplate();
    }

    public static void main(String[] args) {

        boolean[] forks = {true, true, true, true, true};

        Thread phil0 = new Thread(new DiningPhilosophers(0, forks));
        Thread phil1 = new Thread(new DiningPhilosophers(1, forks));
        Thread phil2 = new Thread(new DiningPhilosophers(2, forks));
        Thread phil3 = new Thread(new DiningPhilosophers(3, forks));
        Thread phil4 = new Thread(new DiningPhilosophers(4, forks));

        phil0.start();
        phil1.start();
        phil2.start();
        phil3.start();
        phil4.start();
    }


}
