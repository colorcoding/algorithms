public class RateLimitingTokenBucket {

    /**
     * Requirements
     * 1. Max tokens bucket can hold - N
     * 2. One thread that adds token at 1token/ sec
     * 3. Multiple threads can get a token when it is available
     * 4. Thread should be blocked if no token is available
     */


    private int maxTokens = 0;
    private int tokensAccessed = 0;
    private long startTime;

    public RateLimitingTokenBucket(int maxTokens) {
        this.maxTokens = maxTokens;
        this.startTime = System.currentTimeMillis();
    }

    public synchronized void getToken() throws InterruptedException {
        long totalTokens = (System.currentTimeMillis() - this.startTime) /1000;

        if ((totalTokens - tokensAccessed) == 0) {
            Thread.sleep(1000);
        }
        tokensAccessed++;
    }
}
