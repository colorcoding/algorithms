package leetcode;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Leetcode problem number 1125
 * Minimum developers required
 */
public class SmallestSufficientTeam {

    /**
     * Create the mask - binary representation of skills of a person
     * The 1s in mask - skills of a person. The position of bits in mask
     * is corresponding to index of req_skills array.
     *
     * 1. Map the skills to the index in req_skills
     * 2. Create the mask
     */

    public int[] smallestSufficientTeam(String[] req_skills, List<List<String>> people) {
        int[] result = null;

        int[] masks = new int[people.size()];
        // Map the skills to the index in req_skills
        for(int i = 0; i < people.size(); i++) {
            masks[i] = createMask(req_skills, people.get(i));
        }

        // get smallest team from the masks by OR(ing) them with each other
        // DP and memoization

        return result;
    }

    private int createMask(String[] req_skills, List<String> skills) {
        int mask = 0;
        Map<String, Integer> skillMap = getSkillMap(req_skills);
        for(String skill : skills) {
            mask = mask | (1 << skillMap.get(skill));
        }
        return mask;
    }

    private Map<String, Integer> getSkillMap(String[] req_skills) {
        Map<String, Integer> skillMap = new HashMap<>();
        for (int i = 0; i < req_skills.length; i++) {
            skillMap.put(req_skills[i], i);
        }
        return skillMap;
    }


    public static void main(String[] args) {

    }
}
